#include "list.hpp"
#include <string>

int main(int argc, char **argv) {

  List test = List(10);

  std::string value1 = "1";
  std::string value2 = "2";
  std::string value3 = "3";
  std::string value4 = "4";
  std::string value5 = "5";
  std::string value6 = "6";

  test.print();
  test.insertBegin(&value1);
  test.print();
  test.insertBegin(&value2);
  test.print();
  test.insertBegin(&value3);
  test.print();
  test.insertEnd(&value4);
  test.print();
  test.insertEnd(&value5);
  test.print();
  test.insertI(&value6, 6);
  test.print();
  test.deleteBegin();
  test.print();
  test.deleteEnd();
  test.print();
  test.deleteI(1);
  test.print();
  test.deleteI(2);
  test.print();
  test.deleteI(50);
  test.print();
  test.deleteI(3);
  test.print();
  test.deleteI(1);
  test.print();
  test.deleteI(1);
  test.print();

  return 0;
}
