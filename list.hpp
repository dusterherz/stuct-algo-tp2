#ifndef LIST_HPP
#define LIST_HPP

#include <cstdio>
#include <iostream>

class Element {

  public:
    void *element;
    int next;
    int prev;
    bool empty;

    Element();
};

class List {
  private:
    size_t size;
    Element *elements;
    int start;
    int end;

    int findFree();

  public:
    List(size_t size);
    ~List();

    void insertBegin(void *element);
    void insertEnd(void *element);
    void insertI(void *element, int position);

    void deleteBegin();
    void deleteEnd();
    void deleteI(int position);

    void print();
};

#endif
