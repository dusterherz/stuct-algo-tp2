#include "list.hpp"

Element::Element() {
  this->element = NULL;
  this->next = -1;
  this->prev = -1;
  this->empty = true;
}

List::List(size_t size) {
  this->size = size;
  this->elements = new Element[size];
  for (int i = 0; i < size; i++) {
    this->elements[i] = Element();
  }
  this->start = -1;
  this->end = -1;
}

List::~List() {
  delete this->elements;
}

int List::findFree() {
  int i = 0;
  while (this->elements[i].empty != true && i < this->size)
    i++;
  if (i == this->size)
    return -1;
  else
    return i;
}

void List::insertBegin(void *element) {
  int i = this->findFree();

  if (i == -1) {
    throw "no more space";
  }
  this->elements[i].element = element;
  this->elements[i].empty = false;
  this->elements[i].next = this->start;
  this->elements[i].prev = -1;
  if (this->start != -1)
    this->elements[this->start].prev = i;
  else
    this->end = i;
  this->start = i;
}

void List::insertEnd(void *element) {
  int i = this->findFree();

  if (i == -1) {
    throw "no more space";
  }
  this->elements[i].element = element;
  this->elements[i].empty = false;
  this->elements[i].next = -1;
  this->elements[i].prev = this->end;
  if (this->end != -1) {
    this->elements[this->end].next = i;
  }
  else
    this->start = i;
  this->end = i;
}

void List::insertI(void *element, int position) {
  position--;
  if (position < 0 && position > this->size) {
    throw "wrong position";
  }
  int i = this->findFree();
  if (i == -1) {
    throw "no more space";
  }
  int iter = this->start;
  for (int j = 0; j < position; j++) {
    iter = this->elements[iter].next;
    if (this->elements[iter].next == -1 && position > j + 1) {
      this->insertEnd(element);
      return;
    }
  }
  if (iter == this->start) {
    this->insertBegin(element);
  }
  else {
    this->elements[i] = this->elements[iter];
    this->elements[i].empty = false;
    this->elements[i].element = element;
    this->elements[i].next = iter;
    this->elements[iter].prev = i;
    this->elements[this->elements[i].prev].next = i;
  }
}

void List::deleteBegin() {
  if (this->start < 0)
    return;
  this->elements[this->start].empty = true;
  this->start = this->elements[this->start].next;
  if (this->start < 0)
    return;
  this->elements[this->start].prev = -1;
}

void List::deleteEnd() {
  if (this->end < 0)
    return;
  this->elements[this->end].empty = true;
  this->end = this->elements[this->end].prev;
  if (this->end < 0)
    return;
  this->elements[this->end].next = -1;
}

void List::deleteI(int position) {
  if (position < 0 && position >= this->size) {
    throw "wrong position";
  }
  position--;

  int iter = this->start;
  for (int j = 0; j < position && this->elements[iter].next != -1; j++) {
    iter = this->elements[iter].next;
  }
  if (iter == this->start) {
    this->deleteBegin();
  }
  else if (iter == this->end + 1) {
    this->deleteEnd();
  }
  else {
    this->elements[iter].empty = true;
    this->elements[this->elements[iter].prev].next = this->elements[iter].next;
    this->elements[this->elements[iter].next].prev = this->elements[iter].prev;
  }
}

void List::print() {
  int iter = this->start;
  while (iter != -1) {
    std::string *iptr = (std::string *)this->elements[iter].element;
    std::cout << *iptr << "|";
    iter = this->elements[iter].next;
  }
  std::cout << std::endl;
}
